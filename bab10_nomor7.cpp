// oleh Jessy Faujiyyah Khairani (1207050137)
//soal bab 10 nomor 7

//Tulislah prosedur yang menerima nama hari ssekarang dan menentukan nama hari besok. 
//Misalnya, jika hari sekarang "rabu" maka hari esok adalah "kamis"

#include <iostream>
#include <conio.h>
using namespace std;

void nama_hari (){
	int hari;
	cout<<"Program Pemilihan Hari"<<endl;
	cout<<"1. Senin"<<endl;
	cout<<"2. Selasa"<<endl;
	cout<<"3. Rabu"<<endl;
	cout<<"4. Kamis"<<endl;
	cout<<"5. Jumat"<<endl;
	cout<<"6. Sabtu"<<endl;
	cout<<"7. Minggu"<<endl;
	cout<<"Masukan hari ini (1-7):";
	cin>>hari;	

	switch (hari){
		case 1: 
		cout<<"Hari ini Senin, maka besok adalah hari Selasa"<<endl;
		break;
		case 2: 
		cout<<"Hari ini Selasa, maka besok adalah hari Rabu"<<endl;
		break;
		case 3: 
		cout<<"Hari ini Rabu, maka besok adalah hari Kamis"<<endl;
		break;
		case 4: 
		cout<<"Hari ini Kamis, maka besok adalah hari Jumat"<<endl;
		break;
		case 5: 
		cout<<"Hari ini Jumat, maka besok adalah hari Sabtu"<<endl;
		break;
		case 6: 
		cout<<"Hari ini Sabtu, maka besok adalah hari Minggu"<<endl;
		break;
		case 7: 
		cout<<"Hari ini Minggu, maka besok adalah hari Senin"<<endl;
		break;
		default : cout<<"Hari yang anda masukkan salah"<<endl;
	}
}

int main(){
	nama_hari();
	return 0;
}

	
