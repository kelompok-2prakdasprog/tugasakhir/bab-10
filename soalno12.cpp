#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
	int baris;

	cout << "Masukkan jumlah baris : ";
	cin>>baris;
	cout << endl;


	for (int i = 1; i <= baris; i++)
	{
		for (int j = baris; j > i; j--)
		{
			cout << " ";
		}
		for (int k = 1; k <= (2*i-1); k++)
		{
			cout<<"x";		
		}
		cout<<endl;
	}
	return 0;
}