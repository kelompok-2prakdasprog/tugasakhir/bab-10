//Nama : Dian Hasna Ramadhani
//NIM : 1207050026
//Kelas : Teknik Informstika / B

//BAB 10
//9. Tulislah prosedur yang menerima sebuah tanggal dalam bentuk dd-mm-yyyy (contoh : 12-8-1996) dan memberikan luaran tanggal sebelumnya. Catatan : parameter tanggal berjenis masukan / keluaran.
//Jawab :
#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int tgl,bln,thn,max_tgl;

	cout<<"Masukan tanggal hari ini :\n"<<endl;
	cout<<"Tanggal :";
	cin>>tgl;
	cout<<"Bulan :";
	cin>>bln;
	cout<<"Tahun :";
	cin>>thn;

	if((tgl>0)&&(tgl<=31)&&(bln>0)&&(bln<=12)&&(thn>0))
	{
			tgl=tgl-1;
			bln=bln-1;
			thn=thn;
		cout<<"\n Maka kemarin adalah tanggal "<<endl;
		cout<<tgl<<"-"<<bln<<"-"<<thn;
	}
else
{
	cout<<"Harap masukan tanggal yang benar";
}
	return 0;
}
