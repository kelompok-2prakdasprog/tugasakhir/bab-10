// oleh Jessy Faujiyyah Khairani (1207050137)
//soal bab 10 nomor 14

//Buatlah prosedur dalam bahasa c++ untuk mencetak gambar seperti di bawah ini
//			*
//		*	*	*
//	*	*	*	*	*
//*	*	*	*	*	*	*
//	*	*	*	*	*
//		*	*	*
//			*
// jika diberikan nilai N(yaitu lebar baris terpanjang), dengan asumsi N adalah ganjil.


#include <iostream>
using namespace std;

void segitiga1 ()
	{ int n,i,j,k,l,segitiga1;
	n=4;
		for (i=1;i<=n;i++){
		for(j=i;j<=n-1;j++)
			cout<<"  ";
		for(k=1; k<=i; k++)
			cout<<"* ";
		for(l=2; l<=i; l++)
			cout<<"* ";
			cout<<endl;
		} 
	}
//==============
void segitiga2 ()
	{ int n,i,j,k,l;
	n=4;
		for (i=n-1;i>=1;i--){
		for(j=i;j<=n-1;j++)
			cout<<"  ";
		for(k=1; k<=i; k++)
			cout<<"* ";
		for(l=2; l<=i; l++)
			cout<<"* ";
			cout<<endl;
		} 
	}


int main ()
{ int n=4;
	segitiga1();
	segitiga2();
	return 0;
}
